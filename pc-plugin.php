<?php

/**
 * Plugin Name: PC Plugin
 * Plugin URI: https://preaching.cloud
 * Description: Custom functions for Preaching.Cloud
 * Version: 0.1
 * Author: Preaching.Cloud
 * Author URI: https://preaching.cloud
 * License: License: GPL2+
 * Bitbucket Plugin URI: https://bitbucket.org/jackfelton/pc-plugin
**/

/*  Copyright 2017 preaching.cloud  (email: hello@preaching.cloud)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License, version 2, as 
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

!defined( 'ABSPATH' ) AND exit("Oops!");

// Paths
define( 'PC_FILE', 		__FILE__ );											// plugin's main file absolute path
define( 'PC_FILE_BASE', 	plugin_basename( PC_FILE ) );						// plugin's main file path relative to plugin directory
define( 'PC_DIR', 			dirname( PC_FILE_BASE ) );							// plugin's directory
define( 'PC_PATH',			untrailingslashit( plugin_dir_path( PC_FILE ) ) );	// plugin's absolute path
define( 'PC_URL', 			untrailingslashit( plugin_dir_url( PC_FILE ) ) );	// plugin's directory URL

// Directories
define( 'PC_INC_DIR',		'includes' );					// includes directory
define( 'PC_ADMIN_DIR',	PC_INC_DIR . '/admin' );		// admin directory
define( 'PC_CLASS_DIR', 	PC_INC_DIR . '/classes' );		// classes directory
define( 'PC_LIB_DIR', 		PC_INC_DIR . '/libraries' );	// libraries directory
define( 'PC_CSS_DIR', 		'css' );						// stylesheets directory
define( 'PC_JS_DIR', 		'js' );							// JavaScript directory
define( 'PC_IMG_DIR', 		'images' );						// images directory
define( 'PC_LANG_DIR', 	'languages' );					// languages directory

// Includes
require_once PC_INC_DIR . '/types-taxonomies.php';
require_once PC_ADMIN_DIR . '/process-fields.php';
require_once PC_ADMIN_DIR . '/metaboxes.php';
require_once PC_ADMIN_DIR . '/admin-columns.php';
require_once PC_ADMIN_DIR . '/file-handling.php';

/**
 * Show array as HTML
 * This is helpful for development / debugging
 *
 */
function pc_print_array( $array, $return = false ) {

	$result = '<pre>' . print_r( $array, true ) . '</pre>';

	if ( empty($return) ) {
		echo $result;
	} else {
		return $result;
	}

}
?>
