<?php
/**
 * Special processing for sermon custom fields
 * TODO save Bible books to separate custom fields for easier sorting
 */

 // Save ACF custom fields to date and content of post
function pc_save_sermon_date( $post_id ) {
	
	if ( 'pc_sermon' !== get_post_type( $post_id ) )
				return;
	//echo "processing..."; die;
	$new_date = get_field('pc_date', $post_id);
	$sermon_date = pc_convert_to_datetime ( $new_date, $time );
	$sermon_content = get_field('pc_manuscript', $post_id);
	
	$sermon_post = array(
      'ID'           => $post_id,
      'post_date'   => $sermon_date,
      'post_content' => $sermon_content,
  	);
		wp_update_post( $sermon_post );
	//pc_print_array( $sermon_post ); die;
	
}
add_action('acf/save_post', 'pc_save_sermon_date', 20);

/**
 * Convert date and time to MySQL DATETIME format
 *
 * If no date, value will be 0000-00-00 00:00:00
 * If no time, value will be 2014-10-28 00:00:00
 *
 * @since 1.2
 * @param string $date Date in YYYY-mm-dd format (e.g. 2014-05-10 for May 5th, 2014)
 * @param string $time Time in 24-hour hh-mm format (e.g. 08:00 for 8 AM or 13:12 for 1:12 PM)
 * @return string Date and time in DATETIME format (e.g. 2014-05-10 13:12:00)
 */
function pc_convert_to_datetime( $date, $time ) {

	if ( empty( $date ) ) {
		$date = '0000-00-00';
	}
	if ( empty( $time ) ) {
		$time = '00:00';
	}

	$datetime = $date . ' ' . $time . ':00';

	return apply_filters( 'pc_convert_to_datetime', $datetime, $date, $time );

}


 /**
  * Save enclosure for sermon podcasting
  *
  * When audio URL is provided, save its data to the 'enclosure' field.
  * WordPress automatically uses this data to make feeds useful for podcasting.
  *
  *
  * @param int $post_id ID of post being saved
  * @param object $post Post object being saved
  */
 function pc_sermon_save_audio_enclosure( $post_id, $post ) {

 	// Stop if no post, auto-save (meta not submitted) or user lacks permission
 	$post_type = get_post_type_object( $post->post_type );
 	if ( empty( $_POST ) || ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) || ! current_user_can( $post_type->cap->edit_post, $post_id ) ) {
 		return false;
 	}

 	// Stop if PowerPress plugin is active
 	// Solves conflict regarding enclosure field: http://wordpress.org/support/topic/breaks-blubrry-powerpress-plugin?replies=6
 	if ( defined( 'POWERPRESS_VERSION' ) ) {
 		return false;
 	}

 	// Get audio URL
 	$audio = get_field('pc_sermon_audio');

 	// Populate enclosure field with URL, length and format, if valid URL found
 	do_enclose( $audio, $post_id );

 }

 add_action( 'save_post', 'pc_sermon_save_audio_enclosure', 11, 2 ); // after 'save_post' saves meta fields on 10

?>
