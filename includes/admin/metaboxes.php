<?php
// speed up admin side with filter
add_filter('acf/settings/remove_wp_meta_box', '__return_true');

// Add content to the publish meta box; TODO add relevant content :-)
function pc_add_publish_meta_options($post_obj) {
 
  global $post;
  $post_type = 'pc_sermon'; // If you want a specific post type
  //$value = get_post_meta($post_obj->ID, 'check_meta', true); // If saving value to post_meta
 
  if($post_type==$post->post_type) {
    do_action('pc_publish_meta_box');
		echo  
			'<div class="misc-pub-section misc-pub-section-last">'
      .'<p>'
			.'Here you can read some example text relevant to publishing the sermon.'
			.'</p>'   
			//.'<label><input type="checkbox"' . (!empty($value) ? ' checked="checked" ' : null) . ' value="1" name="check_meta" /> Check meta</label>'
         .'</div>';
  }
}
 
// Add the extra options to the 'Publish' box
add_action('post_submitbox_misc_actions', 'pc_add_publish_meta_options');

// hide stuff from publish box
function pc_hide_publishing_actions(){
        $my_post_type = 'pc_sermon';
        global $post;
        if($post->post_type == $my_post_type){
            echo '
                <style type="text/css">
                    .misc-pub-post-status, 
										.misc-pub-visibility,
										.misc-pub-curtime
										{ display:none; }
                </style>
            ';
        }
}
add_action('admin_head-post.php', 'pc_hide_publishing_actions');
add_action('admin_head-post-new.php', 'pc_hide_publishing_actions');

/**
 * Add meta boxes for sermon custom fields
 **/

add_action( 'acf/init', 'pc_add_sermon_metaboxes' );

function pc_add_sermon_metaboxes() {

acf_add_local_field_group(array (
	'key' => 'group_5924a9f678682',
	'title' => 'Sermon Fields',
	'fields' => array (
		array (
			'key' => 'field_5924aa5c88f20',
			'label' => 'Date',
			'name' => 'pc_date',
			'type' => 'date_picker',
			'instructions' => 'Pick the date this sermon was preached.',
			'required' => 1,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'display_format' => 'F j, Y',
			'return_format' => 'Y-m-d',
			'first_day' => 0,
		),
		array (
			'key' => 'field_5924aad388f21',
			'label' => 'Bible References',
			'name' => 'pc_bible',
			'type' => 'repeater',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'collapsed' => '',
			'min' => 1,
			'max' => 0,
			'layout' => 'block',
			'button_label' => 'Add Bible Passage',
			'sub_fields' => array (
				array (
					'key' => 'field_5924ac8a88f43',
					'label' => 'Bible Book',
					'name' => 'pc_book',
					'type' => 'taxonomy',
					'instructions' => 'Select the book of the Bible',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '45',
						'class' => '',
						'id' => '',
					),
					'taxonomy' => 'pc_sermon_book',
					'field_type' => 'select',
					'allow_null' => 0,
					'add_term' => 1,
					'save_terms' => 1,
					'load_terms' => 0,
					'return_format' => 'object',
					'multiple' => 0,
				),
				array (
					'key' => 'field_5924acd188f23',
					'label' => 'Chapter and Verse',
					'name' => 'pc_reference',
					'type' => 'text',
					'instructions' => '"3:16-19" not "John 3:16-19"',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '45',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => 'Do not include the book name here',
					'prepend' => '',
					'append' => '',
					'maxlength' => '',
				),
			),
		),
		array (
			'key' => 'field_5924aec57ce14',
			'label' => 'Speaker',
			'name' => 'pc_speaker',
			'type' => 'taxonomy',
			'instructions' => 'Speakers can be added or edited by using the menu on the left.',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'taxonomy' => 'pc_sermon_speaker',
			'field_type' => 'select',
			'allow_null' => 1,
			'add_term' => 1,
			'save_terms' => 1,
			'load_terms' => 1,
			'return_format' => 'object',
			'multiple' => 0,
		),
		array (
			'key' => 'field_5924af187ce15',
			'label' => 'Series',
			'name' => 'pc_series',
			'type' => 'taxonomy',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'taxonomy' => 'pc_sermon_series',
			'field_type' => 'select',
			'allow_null' => 1,
			'add_term' => 1,
			'save_terms' => 1,
			'load_terms' => 1,
			'return_format' => 'object',
			'multiple' => 0,
		),
		array (
			'key' => 'field_5924af3a7ce16',
			'label' => 'Topics',
			'name' => 'pc_topics',
			'type' => 'taxonomy',
			'instructions' => 'Assign topics to your sermon so others can easily find a helpful message.',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'taxonomy' => 'pc_sermon_topic',
			'field_type' => 'multi_select',
			'allow_null' => 1,
			'add_term' => 1,
			'save_terms' => 1,
			'load_terms' => 1,
			'return_format' => 'id',
			'multiple' => 0,
		),
		array (
			'key' => 'field_5924c93984cdb',
			'label' => 'Video',
			'name' => 'pc_sermon_video',
			'type' => 'oembed',
			'instructions' => 'Upload a video to a supported site, then paste its URL here.',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'width' => '',
			'height' => '',
		),
		array (
			'key' => 'field_5924b472121e5',
			'label' => 'Audio',
			'name' => 'pc_sermon_audio',
			'type' => 'file',
			'instructions' => 'Upload your sermon as an MP3 file.',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'return_format' => 'url',
			'library' => 'uploadedTo',
			'min_size' => '',
			'max_size' => '',
			'mime_types' => 'mp3',
		),
		array (
			'key' => 'field_5924ca2c84cdc',
			'label' => 'PDF',
			'name' => 'pc_sermon_pdf',
			'type' => 'file',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'return_format' => 'url',
			'library' => 'uploadedTo',
			'min_size' => '',
			'max_size' => '',
			'mime_types' => 'pdf',
		),
		array (
			'key' => 'field_5924ca6384cdd',
			'label' => 'Full Text?',
			'name' => 'pc_sermon_has_full_text',
			'type' => 'checkbox',
			'instructions' => 'Check this if the sermon has a full manuscript.',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				'Yes' => 'Yes, I want to add the manuscript.',
			),
			'allow_custom' => 0,
			'save_custom' => 0,
			'default_value' => array (
			),
			'layout' => 'vertical',
			'toggle' => 0,
			'return_format' => 'value',
		),
		array (
			'key' => 'field_5924cab684cde',
			'label' => 'Manuscript',
			'name' => 'pc_manuscript',
			'type' => 'wysiwyg',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_5924ca6384cdd',
						'operator' => '==',
						'value' => 'Yes',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'tabs' => 'all',
			'toolbar' => 'basic',
			'media_upload' => 0,
			'delay' => 0,
		),
		array (
			'key' => 'field_5924af717ce17',
			'label' => 'Tags',
			'name' => 'pc_tags',
			'type' => 'taxonomy',
			'instructions' => 'Tags are useful to organize sermons. For example, you may want to distinguish different service types by using a unique tag for each: "Sunday AM", "Sunday PM", etc.',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'taxonomy' => 'pc_sermon_tag',
			'field_type' => 'multi_select',
			'allow_null' => 1,
			'add_term' => 1,
			'save_terms' => 1,
			'load_terms' => 1,
			'return_format' => 'id',
			'multiple' => 0,
		),
	),
	'location' => array (
		array (
			array (
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'pc_sermon',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'acf_after_title',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => array (
		0 => 'the_content',
		1 => 'excerpt',
		2 => 'custom_fields',
		3 => 'revisions',
		4 => 'author',
		5 => 'format',
		6 => 'page_attributes',
		7 => 'featured_image',
		8 => 'categories',
		9 => 'tags',
		10 => 'send-trackbacks',
	),
	'active' => 1,
	'description' => '',
));

}

?>