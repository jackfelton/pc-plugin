<?php
/*
Allow uploading PDF, Presentations (pptx) and Documents (doc, docx)
Author: Jörn Lund
Author URI: http://github.org/mcguffin
Version: 0.0.1
*/
function pc_enable_documents_upload( $post_mime_types ) {
    $post_mime_types['application/pdf'] = array( __( 'PDFs' , 'mu-plugins' ), __( 'Manage PDFs' , 'mu-plugins'), _n_noop( 'PDF <span class="count">(%s)</span>', 'PDFs <span class="count">(%s)</span>' , 'mu-plugins') );
    $post_mime_types['application/vnd.openxmlformats-officedocument.presentationml.presentation'] = array( __( 'Presentations', 'mu-plugins' ), __( 'Manage Presentations', 'mu-plugins' ), _n_noop( 'Powerpoints <span class="count">(%s)</span>', 'Powerpoints <span class="count">(%s)</span>', 'mu-plugins' ) );
    $post_mime_types['application/msword, application/vnd.openxmlformats-officedocument.wordprocessingml.document'] = array( __( 'Documents', 'mu-plugins' ), __( 'Manage Documents', 'mu-plugins' ), _n_noop( 'Documents <span class="count">(%s)</span>', 'Documents <span class="count">(%s)</span>', 'mu-plugins' ) );
    return $post_mime_types;
}
add_filter( 'post_mime_types', 'pc_enable_documents_upload' );

/*
Resize Images in the Browser before they get uploaded.
Author: Jörn Lund
Author URI: http://github.org/mcguffin
Version: 0.0.2
*/
/**
 * Return all available image sizes.
 * (Copied from http://codex.wordpress.org/Function_Reference/get_intermediate_image_sizes)
 *
 */
if ( ! function_exists('get_image_sizes') ) :
function get_image_sizes( $size = '' ) {
	global $_wp_additional_image_sizes;
	$sizes = array();
	$get_intermediate_image_sizes = get_intermediate_image_sizes();
	// Create the full array with sizes and crop info
	foreach( $get_intermediate_image_sizes as $_size ) {
		if ( in_array( $_size, array( 'thumbnail', 'medium', 'large' ) ) ) {
			$sizes[ $_size ]['width'] = get_option( $_size . '_size_w' );
			$sizes[ $_size ]['height'] = get_option( $_size . '_size_h' );
			$sizes[ $_size ]['crop'] = (bool) get_option( $_size . '_crop' );
		} elseif ( isset( $_wp_additional_image_sizes[ $_size ] ) ) {
			$sizes[ $_size ] = array( 
			'width' => $_wp_additional_image_sizes[ $_size ]['width'],
			'height' => $_wp_additional_image_sizes[ $_size ]['height'],
			'crop' =>  $_wp_additional_image_sizes[ $_size ]['crop']
			);
		}
	}
	// Get only 1 size if found
	if ( $size ) {
		if( isset( $sizes[ $size ] ) ) {
			return $sizes[ $size ];
		} else {
			return false;
		}
	}
	return $sizes;
}
endif;
/*
This one works on the Media upload page.
*/
function pc_client_side_resize_plupload_params( $params ) {
	// get biggest possible image
	$sizes = get_image_sizes();
	$largest = array( 'width'=>0 , 'height'=>0 );
	foreach ( $sizes as $size ) {
		$largest['width'] = max($size['width'],$largest['width']);
 		$largest['height'] = max($size['height'],$largest['height']);
	}
	$params['resize'] = array(
		'enabled' => true,
		'width'		=> $largest['width'],
		'height'	=> $largest['height'],
		'quality'	=> 90
	);
	return $params;
}
add_filter( 'plupload_init', 'pc_client_side_resize_plupload_params' , 20);
/*
This one is for the js media library
*/
function pc_client_side_resize_load() {
	wp_enqueue_script( 'client-resize' , trailingslashit( PC_JS_DIR ) . 'client-side-image-resize.js', array('media-editor' ) , '0.0.1' );
	wp_localize_script( 'client-resize' , 'client_resize' , array( 
		'plupload' => pc_client_side_resize_plupload_params( array() ) 
	) );
}
add_action( 'wp_enqueue_media' , 'pc_client_side_resize_load' );

/**
 * Get sermon from audio file
 * @param  string $file File name & path
 * @return object       Sermon post object
 */
function pc_get_sermon_from_file( $file = '' ) {
  global $post;
  $sermon = false;
  if ( $file != '' ) {
    $args = array(
      'post_type' => 'pc_sermon',
      'post_status' => 'publish',
      'posts_per_page' => 1,
      'meta_key' => 'pc_sermon_audio',
      'meta_value' => $file
    );
    $qry = new WP_Query( $args );
    if ( $qry->have_posts() ) {
      while ( $qry->have_posts() ) { $qry->the_post();
        $sermon = $post;
        break;
      }
    }
  }
  return apply_filters( 'pc_sermon_from_file', $sermon, $file );
}

/**
 * Get size of media file
 * @param  string  $file File name & path
 * @return boolean       File size on success, boolean false on failure
 */
function pc_get_file_size( $file = '' ) {
  if ( $file ) {
    // Include media functions if necessary
    if ( ! function_exists( 'wp_read_audio_metadata' ) ) {
      require_once( ABSPATH . 'wp-admin/includes/media.php' );
    }
    // translate file URL to local file path if possible
    $file = pc_get_local_file_path( $file );
    // Get file data (for local file)
    $data = wp_read_audio_metadata( $file );
    $raw = $formatted = '';
    if ( $data ) {
      $raw = $data['filesize'];
      $formatted = pc_format_bytes( $raw );
    } else {
      // get file data (for remote file)
      $data = wp_remote_head( $file, array( 'timeout' => 10, 'redirection' => 5 ) );
      if ( ! is_wp_error( $data ) && is_array( $data ) && isset( $data['headers']['content-length'] ) ) {
        $raw = $data['headers']['content-length'];
        $formatted = pc_format_bytes( $raw );
      }
    }
    if ( $raw || $formatted ) {
      $size = array(
        'raw' => $raw,
        'formatted' => $formatted
      );
      return apply_filters( 'pc_file_size', $size, $file );
    }
  }
  return false;
}
/**
 * Returns a local file path for the given file URL if it's local. Otherwise
 * returns the original URL
 *
 * @param    string    file
 * @return   string    file or local file path
 */
function pc_get_local_file_path( $file ) {
  // Identify file by root path and not URL (required for getID3 class)
  $site_root = trailingslashit( ABSPATH );
  $site_url = trailingslashit( site_url() );
  // Remove common dirs from the ends of site_url and site_root, so that file can be outside of the WordPress installation
  $root_chunks = explode( '/', $site_root );
  $url_chunks  = explode( '/', $site_url );
  end( $root_chunks );
  end( $url_chunks );
  while ( ! is_null( key( $root_chunks ) ) && ! is_null( key( $url_chunks ) ) && ( current( $root_chunks ) == current( $url_chunks ) ) ) {
    array_pop( $root_chunks );
    array_pop( $url_chunks );
    end( $root_chunks );
    end( $url_chunks );
  }
  $site_root = implode('/', $root_chunks);
  $site_url  = implode('/', $url_chunks);
  $file = str_replace( $site_url, $site_root, $file );
  return $file;
}
/**
 * Get duration of audio file
 * @param  string $file File name & path
 * @return mixed        File duration on success, boolean false on failure
 */
function pc_get_file_duration( $file ) {
  if ( $file ) {
    // Include media functions if necessary
    if ( ! function_exists( 'wp_read_audio_metadata' ) ) {
      require_once( ABSPATH . 'wp-admin/includes/media.php' );
    }
    // translate file URL to local file path if possible
    $file = pc_get_local_file_path( $file );
    // Get file data (will only work for local files)
    $data = wp_read_audio_metadata( $file );
    $duration = false;
    if ( $data ) {
      if ( isset( $data['length_formatted'] ) && strlen( $data['length_formatted'] ) > 0 ) {
        $duration = $data['length_formatted'];
      } else {
        if ( isset( $data['length'] ) && strlen( $data['length'] ) > 0 ) {
          $duration = gmdate( 'H:i:s', $data['length'] );
        }
      }
    }
    if ( $data ) {
      return apply_filters( 'pc_file_duration', $duration, $file );
    }
  }
  return false;
}
/**
 * Format filesize for display
 * @param  integer $size      Raw file size
 * @param  integer $precision Level of precision for formatting
 * @return mixed              Formatted file size on success, false on failure
 */
function pc_format_bytes( $size , $precision = 2 ) {
  if ( $size ) {
      $base = log ( $size ) / log( 1024 );
      $suffixes = array( '' , 'k' , 'M' , 'G' , 'T' );
      $formatted_size = round( pow( 1024 , $base - floor( $base ) ) , $precision ) . $suffixes[ floor( $base ) ];
      return apply_filters( 'pc_file_size_formatted', $formatted_size, $size );
  }
  return false;
}

  
?>