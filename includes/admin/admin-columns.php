<?php
// No direct access
if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Get taxonomy term list for a post type with admin links
 *
 * @param int $post_id Post ID to get term list for
 * @param string $taxonomy Taxonomy to get terms for
 * @return string Term list
 */
function pc_admin_term_list( $post_id, $taxonomy ) {

	$list = '';

	// Get taxonomy and output a list
	$terms = get_the_terms( $post_id, $taxonomy );

	if ( $terms && ! is_wp_error( $terms ) ) {

		$post_type = get_post_type( $post_id );

		$terms_array = array();

		foreach ( $terms as $term ) {
			$terms_array[] = '<a href="' . esc_url( admin_url( 'edit.php?' . $taxonomy . '=' . $term->slug  . '&post_type=' . $post_type ) ) . '"> ' . $term->name . '</a>';
		}

		$list = implode( ', ', $terms_array );

	}

	return apply_filters( 'pc_admin_term_list', $list, $post_id, $taxonomy );

}

?>