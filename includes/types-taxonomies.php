<?php

// No direct access
if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Post Type: Sermons
 */
function pc_register_cpt_pc_sermon() {

	$labels = array(
		"name" => __( 'Sermons', 'pc-church' ),
		"singular_name" => __( 'Sermon', 'pc-church' ),
		"menu_name" => __( 'Sermons', 'pc-church' ),
		"all_items" => __( 'All Sermons', 'pc-church' ),
		"add_new" => __( 'Add New', 'pc-church' ),
		"add_new_item" => __( 'Add New Sermon', 'pc-church' ),
		"edit_item" => __( 'Edit Sermon', 'pc-church' ),
		"new_item" => __( 'New Sermon', 'pc-church' ),
		"view_item" => __( 'View Sermon', 'pc-church' ),
		"view_items" => __( 'View Sermons', 'pc-church' ),
		"search_items" => __( 'Search Sermons', 'pc-church' ),
		"not_found" => __( 'No Sermons Found', 'pc-church' ),
		"not_found_in_trash" => __( 'No Sermons found in the Trash', 'pc-church' ),
		"parent_item_colon" => __( 'Parent Sermon:', 'pc-church' ),
		"featured_image" => __( 'Featured Image', 'pc-church' ),
		"set_featured_image" => __( 'Set featured image', 'pc-church' ),
		"remove_featured_image" => __( 'Remove featured image', 'pc-church' ),
		"use_featured_image" => __( 'Use as featured image', 'pc-church' ),
		"archives" => __( 'Sermons', 'pc-church' ),
		"insert_into_item" => __( 'Insert into Sermon', 'pc-church' ),
		"uploaded_to_this_item" => __( 'Upload to this sermon', 'pc-church' ),
		"filter_items_list" => __( 'Filter Sermons list', 'pc-church' ),
		"items_list_navigation" => __( 'Sermon list navigation', 'pc-church' ),
		"items_list" => __( 'Sermon list', 'pc-church' ),
		"attributes" => __( 'Sermon Attributes', 'pc-church' ),
		"parent_item_colon" => __( 'Parent Sermon:', 'pc-church' ),
	);

	$args = array(
		"label" => __( 'Sermons', 'pc-church' ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => true,
		"rest_base" => "sermons",
		"has_archive" => "sermon-archive",
		"show_in_menu" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "sermon", "with_front" => true ),
		"query_var" => true,
		"menu_icon" => "dashicons-controls-volumeon",
		"supports" => array( "title", "editor", "thumbnail" ),
	);

	register_post_type( "pc_sermon", $args );
}

add_action( 'init', 'pc_register_cpt_pc_sermon' );

// Add Taxonomies
function pc_register_my_taxes() {

	/**
	 * Taxonomy: Sermon Topics.
	 */

	$labels = array(
		"name" => __( 'Sermon Topics', 'pc-church' ),
		"singular_name" => __( 'Sermon Topic', 'pc-church' ),
		"menu_name" => __( 'Sermon Topics', 'pc-church' ),
		"all_items" => __( 'All Topics', 'pc-church' ),
		"edit_item" => __( 'Edit Topic', 'pc-church' ),
		"view_item" => __( 'View Topic', 'pc-church' ),
		"update_item" => __( 'Update Topic Name', 'pc-church' ),
		"add_new_item" => __( 'Add New Topic', 'pc-church' ),
		"new_item_name" => __( 'New Topic', 'pc-church' ),
		"parent_item" => __( 'Parent Topic', 'pc-church' ),
		"parent_item_colon" => __( 'Parent Topic:', 'pc-church' ),
		"search_items" => __( 'Search Topics', 'pc-church' ),
		"popular_items" => __( 'Popular Topics', 'pc-church' ),
		"separate_items_with_commas" => __( 'Separate topics with commas', 'pc-church' ),
		"add_or_remove_items" => __( 'Add or Remove Topics', 'pc-church' ),
		"choose_from_most_used" => __( 'Choose from the most used Topics', 'pc-church' ),
		"not_found" => __( 'No Topics found', 'pc-church' ),
		"no_terms" => __( 'No Topics', 'pc-church' ),
		"items_list_navigation" => __( 'Topics list navigation', 'pc-church' ),
		"items_list" => __( 'Topics list', 'pc-church' ),
	);

	$args = array(
		"label" => __( 'Sermon Topics', 'pc-church' ),
		"labels" => $labels,
		"public" => true,
		"hierarchical" => true,
		"label" => "Sermon Topics",
		"show_ui" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => false,
    "meta_box_cb"       => false,
		"query_var" => true,
		"rewrite" => array( 'slug' => 'topic', 'with_front' => true,  'hierarchical' => true, ),
		"show_admin_column" => false,
		"show_in_rest" => true,
		"rest_base" => "topic",
		"show_in_quick_edit" => false,
	);
	register_taxonomy( "pc_sermon_topic", array( "pc_sermon" ), $args );

	/**
	 * Taxonomy: Books.
	 */

	$labels = array(
		"name" => __( 'Books', 'pc-church' ),
		"singular_name" => __( 'Book', 'pc-church' ),
	);

	$args = array(
		"label" => __( 'Books', 'pc-church' ),
		"labels" => $labels,
		"public" => true,
		"hierarchical" => true,
		"label" => "Books",
		"show_ui" => true,
		"show_in_menu" => false,
		"show_in_nav_menus" => false,
    "meta_box_cb"       => false,
		"query_var" => true,
		"rewrite" => array( 'slug' => 'book', 'with_front' => true,  'hierarchical' => true, ),
		"show_admin_column" => false,
		"show_in_rest" => true,
		"rest_base" => "book",
		"show_in_quick_edit" => false,
	);
	register_taxonomy( "pc_sermon_book", array( "pc_sermon" ), $args );

	/**
	 * Taxonomy: Series.
	 */

	$labels = array(
		"name" => __( 'Sermon Series', 'pc-church' ),
		"singular_name" => __( 'Series', 'pc-church' ),
		"menu_name" => __( 'Series', 'pc-church' ),
		"all_items" => __( 'All Series', 'pc-church' ),
		"edit_item" => __( 'Edit Series', 'pc-church' ),
		"view_item" => __( 'View Series', 'pc-church' ),
		"update_item" => __( 'Update Series', 'pc-church' ),
		"add_new_item" => __( 'Add New Series', 'pc-church' ),
		"new_item_name" => __( 'New Series Name', 'pc-church' ),
		"parent_item" => __( 'Parent Series', 'pc-church' ),
		"parent_item_colon" => __( 'Parent Series:', 'pc-church' ),
		"search_items" => __( 'Search Series', 'pc-church' ),
		"popular_items" => __( 'Most Used Series', 'pc-church' ),
		"separate_items_with_commas" => __( 'Separate series with commas', 'pc-church' ),
		"add_or_remove_items" => __( 'Add or Remove Series', 'pc-church' ),
		"choose_from_most_used" => __( 'Choose from the most used Series', 'pc-church' ),
		"not_found" => __( 'No Series found', 'pc-church' ),
		"no_terms" => __( 'No Series', 'pc-church' ),
		"items_list_navigation" => __( 'Series list navigation', 'pc-church' ),
		"items_list" => __( 'Series list', 'pc-church' ),
	);

	$args = array(
		"label" => __( 'Series', 'pc-church' ),
		"labels" => $labels,
		"public" => true,
		"hierarchical" => false,
		"label" => "Series",
		"show_ui" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
    "meta_box_cb"       => false,
		"query_var" => true,
		"rewrite" => array( 'slug' => 'series', 'with_front' => true, ),
		"show_admin_column" => true,
		"show_in_rest" => true,
		"rest_base" => "series",
		"show_in_quick_edit" => false,
	);
	register_taxonomy( "pc_sermon_series", array( "pc_sermon" ), $args );

	/**
	 * Taxonomy: Speakers.
	 */

	$labels = array(
		"name" => __( 'Speakers', 'pc-church' ),
		"singular_name" => __( 'Speaker', 'pc-church' ),
		"menu_name" => __( 'Speakers', 'pc-church' ),
		"all_items" => __( 'All Speakers', 'pc-church' ),
		"edit_item" => __( 'Edit Speaker', 'pc-church' ),
		"view_item" => __( 'View Speaker', 'pc-church' ),
		"update_item" => __( 'Update Speaker Name', 'pc-church' ),
		"add_new_item" => __( 'Add New Speaker', 'pc-church' ),
		"new_item_name" => __( 'New Speaker Name', 'pc-church' ),
		"search_items" => __( 'Search Speakers', 'pc-church' ),
		"popular_items" => __( 'Most Used Speakers', 'pc-church' ),
		"separate_items_with_commas" => __( 'Separate speakers with commas', 'pc-church' ),
		"add_or_remove_items" => __( 'Add or Remove Speakers', 'pc-church' ),
		"choose_from_most_used" => __( 'Choose from the most used Speakers', 'pc-church' ),
		"not_found" => __( 'No Speakers found', 'pc-church' ),
		"no_terms" => __( 'No Speakers', 'pc-church' ),
		"items_list_navigation" => __( 'Speakers list navigation', 'pc-church' ),
		"items_list" => __( 'Speakers list', 'pc-church' ),
	);

	$args = array(
		"label" => __( 'Speakers', 'pc-church' ),
		"labels" => $labels,
		"public" => true,
		"hierarchical" => false,
		"label" => "Speakers",
		"show_ui" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
    "meta_box_cb"       => false,
		"query_var" => true,
		"rewrite" => array( 'slug' => 'speaker', 'with_front' => true, ),
		"show_admin_column" => true,
		"show_in_rest" => true,
		"rest_base" => "speaker",
		"show_in_quick_edit" => false,
	);
	register_taxonomy( "pc_sermon_speaker", array( "pc_sermon" ), $args );

	/**
	 * Taxonomy: Sermon Tags.
	 */

	$labels = array(
		"name" => __( 'Sermon Tags', 'pc-church' ),
		"singular_name" => __( 'Sermon Tag', 'pc-church' ),
		"menu_name" => __( 'Sermon Tags', 'pc-church' ),
		"all_items" => __( 'All Tags', 'pc-church' ),
		"edit_item" => __( 'Edit Tag', 'pc-church' ),
		"view_item" => __( 'View Tag', 'pc-church' ),
		"update_item" => __( 'Update Tag Name', 'pc-church' ),
		"add_new_item" => __( 'Add New Tag', 'pc-church' ),
		"new_item_name" => __( 'New Tag Name', 'pc-church' ),
		"search_items" => __( 'Search Tags', 'pc-church' ),
		"popular_items" => __( 'Popular Tags', 'pc-church' ),
		"separate_items_with_commas" => __( 'Separate tags with commas', 'pc-church' ),
		"add_or_remove_items" => __( 'Add or Remove Tags', 'pc-church' ),
		"choose_from_most_used" => __( 'Choose from the most used Tags', 'pc-church' ),
		"not_found" => __( 'No Tags found', 'pc-church' ),
		"no_terms" => __( 'No Tags', 'pc-church' ),
		"items_list_navigation" => __( 'Tags list navigation', 'pc-church' ),
		"items_list" => __( 'Tags list', 'pc-church' ),
	);

	$args = array(
		"label" => __( 'Sermon Tags', 'pc-church' ),
		"labels" => $labels,
		"public" => true,
		"hierarchical" => false,
		"label" => "Sermon Tags",
		"show_ui" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => false,
    "meta_box_cb"       => false,
		"query_var" => true,
		"rewrite" => array( 'slug' => 'sermon_tag', 'with_front' => true, ),
		"show_admin_column" => false,
		"show_in_rest" => true,
		"rest_base" => "sermon_tag",
		"show_in_quick_edit" => false,
	);
	register_taxonomy( "pc_sermon_tag", array( "pc_sermon" ), $args );
}

add_action( 'init', 'pc_register_my_taxes' );


?>