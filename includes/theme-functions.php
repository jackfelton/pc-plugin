<?php
// write functions for action hooks to add sermon content in a variety of contexts
// single sermon pages
// simple lists of sermon
// other?

function pc_sermon_single ( $post_id ) {
  
}

function pc_sermon_list ( $post_id ) {
  
}

function pc_sermon_archive ( $post_id ) {
  
}

function pc_get_term_image ( $term_id, $image_size {
  // image id is stored as term meta
  $term_id = get_queried_object()->term_id;
  $image_id = get_term_meta( $term_id, 'image', true );

  // image data stored in array, second argument is which image size to retrieve
  $image_data = wp_get_attachment_image_src( $image_id, $image_size );
  
	return apply_filters( 'pc_term_image', $image_data );
}
?>